from flask import Flask 

app = Flask(__name__)
class Blockchain:
	def __init__(self):
		self.transactions = []
		self.chain = []

#instanciacion, se crea un objeto blockchain
blockchain = Blockchain()


#Definicion de las rutas
@app.route('/')
def hello():
	return 'Hola mundo'

if __name__ == '__main__':
	from argparse import ArgumentParser
	parser = ArgumentParser()
	parser.add_argument('-p', '--port', default=5001, type=int, help="Port listener")
	args = parser.parse_args()
	port = args.port
	app.run(host="127.0.0.1", port=port, debug=True)
